---
layout: markdown_page
title: "IAM.1.07 - Shared Account Restrictions Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.1.07 - Shared Account Restrictions

## Control Statement

Where applicable, the use of generic and shared accounts to administer systems or perform critical functions is prohibited; generic user IDs are disabled or removed.

## Context

Use of shared or generic accounts limits the ability to ensure authenticity and integrity.  Someone outside the organization could exploit this and their actions could not be easily traced.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.07_shared_account_restrictions.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.07_shared_account_restrictions.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.07_shared_account_restrictions.md).

## Framework Mapping

* PCI
  * 8.5
