--- 
layout: markdown_page
title: "Vendor Contracts and Invoice Payment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Procure to Pay Process

### Requirement Identification 
Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money). However, any purchases requiring contracts must first be reviewed by legal then signed off by a member of the executive team. Be sure to also check out our guide on [Signing Legal Documents](/handbook/finance/authorization-matrix/#signing-legal-documents/). 

### Vendor and Contract Approval Workflow
#### Step 1: Request Contract from Vendor

Ask your vendor contact to send you a Word document of their contract with the business terms you’ve agreed upon. 

**But wait, is this a SaaS subscription?** If the answer is yes, have you worked with our software buying partner [Vendr](https://www.vendr.com/) to ensure that the quoted pricing is competetive? Vendr can help negotiate directly with vendors on both new subscriptions and renewals. It is preferred that we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors. Please reach out to your Finance Business Partner for an introduction to the team at Vendr. 

##### Addendums to Original Contracts

- If the original contract has already been approved, but there is an addendum that does not change pricing, please open up original issue and attach the addendum to be reviewed by legal and signed by the CFO. Ensure you tag the Finance Buisness Partner so he / she is made aware. There is no need to follow Step 2 onward.

- If the original contract has already been approved, but there is an addendum that **changes** pricing, please create a new issue and follow Step 2 onward. 

#### Step 2: Create a Confidential Issue

 Create a new confidential issue in the Finance Issue Tracker and select the appropriate Vendor Contract Template. Specific templates associated with each function and department can be found below. **Please ensure that you select the appropriate template i.e. G&A, Sales & Marketing, or R&D. This will ensure the appropriate routing of your request.**
 - [General & Administrative Vendor Contract Template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_general_and_adminstrative)
   - Contracts related to the following departments: 
     - Accounting
     - Business Operations
     - CEO
     - Finance
     - Legal
     - People
     - Recruiting

 - [Sales & Marketing Vendor Contract Template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_sales_and_marketing)
    - Contracts related to the following departments: 
      - Sales
        - Alliances
        - Channel
        - Commercial Sales
        - Customer Solutions
        - Customer Success
        - Enterprise Sales
        - Field Operations
      - Marketing
        - Corporate Marketing
        - Demand Generation
        - Digital Marketing
        - Field Marketing
        - Marketing Ops
        - Product Marketing
        - Community Relations

 - [Research & Development Vendor Contract Template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_research_and_development)
    - Contracts related to the following departments: 
      - Engineering
       - Development
       - Quality
       - Security
       - Infrastructure
       - UX
       - Customer Support
     - Meltano
     - Product Management 

If you are requesting approval of a new Master Agreement, please fill out the form completely, with all requested information. Legal will not approve the request if the request is incomplete and missing information. 

For approval of other types of agreements - such as purchase orders on existing agreements, confidentiality agreements, sponsorship agreements, and so forth - contact Legal for a list of necessary information to include with your approval request. 

#### Step 3: Authorizations

Prior to requesting legal review, GitLab team-members must get approval from other interested departments such as the department head of your department, finance (budgetary authorization) and in some cases security. 

1. Please consult the [Authorization Matrix](/handbook/finance/authorization-matrix/) to determine who must sign off on Functional Approval and Financial Approval.  
2. Approval from Finance Ops and Planning for budgeting.
3. Consult the [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit?usp=sharing) to understand whether your contract will need security review. Any contracts that will share RED or ORANGE data will need security approval prior to signing.
4. Complete a [Data Protection Impact Assessment](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment); please note that this will be done in partnership with GitLab's Data Protection Officer and reviewed by Security Compliance during the Security Review.
5. If you are purchasing new software/tools, you also need to get approval from Business Ops.  
  
Assign the issue to the individuals responsible for the necessary authorizations. Authorizations can be completed concurrently. (Do not assign the issue to Legal or the Signer yet.) 

After you have obtained Functional and Financial approval, as well as any Security or Business Ops approval necessary, you are ready to send the contract to Legal.

#### Step 4: Legal Approval and Signing

After authorizations have occurred, tag @gl-legal-team -- confirming the final version of the Vendor contract is attached -- for legal review and approval. 

If the contract cannot be approved, Legal will provide redlines and begin the negotiations with the Vendor. 

Once the contract is approved, Legal will re-assign to you so you can prepare the contract for signing.

1. Create a pdf of the fully approved contract. 
2. Assign your issue to the signer. 
3. Send contract to the signer through your team's shared HelloSign login.

#### Step 5: Obtain Vendor Signatures

Send the GitLab-signed pdf to the vendor through HelloSign.

#### Step 6: Upload Fully Executed Contract to ContractWorks.

You will need to upload the fully signed pdf into your department's folder and then use the Vendor Contract template to add tags, following the provided [instructions and best practices](/handbook/legal/vendor-contract-filing-process)

For users who are uploading a contract for the first time @mention `@gl-legal-team` in a comment to request a ContractWorks sign in. You will receive an invitation to log in and file your fully executed contract. Once credentialized, add your contracts to ContractWorks as soon as a contract has been signed by both parties.

### Invoice Recording and Filing 
Once the invoice is received the person responsible for initiating the request must submit the invoice to ap@gitlab.com along with one of the following:

1. link to the issue that was created to approve the expenditure.
1. If no issue was created, link to uploaded contract in Contractworks ap@gitlab.com.  Accompanying the contract should be email approvals in accorndance with the signature authorization matrix. 

After the invoice has been approved and filed in Google Drive, the Senior Accounts Payable Administrator will record the invoice in NetSuite and move the document to another folder in Google Drive named "Invoice to Pay."

### Payment
The final step in the process is payment. Payment runs are made every Friday. The Senior Accounts Payable Administrator will queue up the payments and send to the Controller for review and approval. Once approved, the Controller submits the payments to the CFO for final approval. Payments are then disbursed and the Senior Accounts Payable Administrator transfers the file to a folder in Google Drive named "Vendor Paid." 
This marks the end of the Procure to Pay process.

