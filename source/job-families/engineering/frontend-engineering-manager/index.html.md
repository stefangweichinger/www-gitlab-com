---
layout: job_family_page
title: "Frontend Engineering Manager"
---

{: .text-center}
<br>

## A brief overview:

GitLab is building an open source, single application for the entire DevOps lifecycle—from project planning and source code management to CI/CD, monitoring, and security.
We already have a large team of Frontend Engineers and we're planning to more than double the team's size over the next year. You can look over our [organizational chart](/company/team/org-chart/) to see open vacancies.
At GitLab, we live and breathe open source principles. This means our entire [handbook](/handbook/) is online, and with a few clicks you can find the details of [upcoming releases](/upcoming-releases/) and an overview of the [product vision](/direction/#product-vision) you’d contribute to when working here.

What you can expect in a Frontend Engineering Manager role at GitLab:

* Actively seek to build out a great team
* Make your team happy and successful
* Improve processes to make your team more effective
* Collaborate effectively with others
* Hold regular 1:1's with all members of your team
* Plan and execute long term strategies that benefit the team and the product stage
* Conduct code reviews, and make technical contributions to product architecture
* Get involved in solving bugs and delivering small features
* Foster technical decision making on the team, but make final decisions when necessary
* Understand engineering metrics and seek to improve them


## Teams you might be a part of:
We have vacancies across our Secure, Defend, Configure & Serverless, Geo, Fulfillment, Ecosystem, and Distribution [stage groups](/handbook/product/categories/#hierarchyhttps://about.gitlab.com/handbook/product/categories/#hierarchy). We work using Agile methodologies and [ship features](/direction/#future-releases) monthly.

## You should apply if:
* You regularly achieve consensus amongst departments and teams
* You have 5 years or more experience in a leadership role with current technical experience
* You have an expert understanding in core web and browser concepts (eg. how does JavaScript handle asynchronous code)
* You have in-depth experience with CSS, HTML, and JavaScript.
* You have excellent written and verbal communication skills
* You share our [values](/handbook/values/), and work in accordance with those values

## Nice to have:
* You have experience contributing to open source software
* You have experience working with modern frontend frameworks (eg. React, Vue.js)
* You have working knowledge of Ruby on Rails
* You have domain knowledge relevant to the product stage in which you are looking to join (eg. someone with CI/CD experience applying for the Verify & Release team)

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to a 30 minute [screening call](/handbook/hiring/#screening-call) with our Technical Recruiters
* Next, candidates will be invited to a 60 minute interview with the Hiring Manager. 
* Candidates will then be invited to a 45 minute peer interview with another Frontend Engineering Manager
* Candidates will then be invited to a 45 minute direct report interview
* Candidates will then be invited to a 60 minute interview with our Senior Director of Development
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## What it’s like to work here at GitLab:

The [culture](/company/culture/) here at GitLab is something we’re incredibly proud of. Because GitLab team-members are currently located in 51 different countries, you’ll spend your time collaborating with kind, talented, and motivated colleagues from across the globe.
Some of the [benefits](/handbook/benefits/) you’ll be entitled to vary by the region or country you’re in. However, all GitLab team-members are fully remote and receive a "no ask, must tell" paid-time-off policy, where we don’t count the number of days you take off annually. You can work incredibly flexible hours, enabled by our asynchronous approach to [communication](/handbook/communication/). Also, every nine months or so, we’ll invite you to our [Contribute](/company/culture/contribute/) event.
Our hiring process for the Frontend Engineer position is consisted of the following [steps](/job-families/engineering/frontend-engineering-manager/). The compensation for the role can be found at the bottom of this [page](/job-families/engineering/frontend-engineering-manager/)
